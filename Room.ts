import { TransformiceClient } from "./Main";

export class TransformiceRoom {
	roomName: string;
	players: {[connectionId: number]: TransformiceClient};

	/**
	 *	Cria uma nova sala
	 */
	constructor(name: string) {
		this.roomName = name;
	}

	/**
	 *	Adiciona o player a sala
	 */
	public addPlayerToRoom(p: TransformiceClient) {
		this.players[p.connectionId] = p;
	}

	public removePlayerFromRoom(p: TransformiceClient) {
		delete this.players[p.connectionId];
	}
}