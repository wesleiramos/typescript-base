/// <reference path="../../../node_modules/@types/node/index.d.ts"/>

import { TransformiceRoom } from "./Room";
import { TransformiceClient } from "./Main";

export class TransformiceServer {

	ids: number;
	connections: {[connectionId: number]: TransformiceClient};
	rooms: {[name: string]: TransformiceRoom};

	ckey: string;
	version: string;

	constructor() {
		this.ids = 0;

		this.ckey = 'vkXdws';
		this.version = '1.400';
	}

	/**
	 *	Quando um novo player for conectado
	 */
	public onClientConnected(p: TransformiceClient) {
		this.ids++;
		p.connectionId = this.ids;
		this.connections[p.connectionId] = p;
	}

	/**
	 *	Quando um player for desconectado
	 */
	public onClientDisconnected(p: TransformiceClient) {
		if (p.room !== undefined) {
			p.room.removePlayerFromRoom(p)
		}

		delete this.connections[p.connectionId];
	}

	/**
	 *	Adiciona um player a sala
	 */
	public addClientToRoom(roomName: string, p: TransformiceClient) {
		// Caso o player já esteve em alguma sala
		if (p.room !== undefined) {
			p.room.removePlayerFromRoom(p);
		}

		if (roomName in this.rooms) {
			p.room = this.rooms[roomName];
		} else {
			this.rooms[roomName] = new TransformiceRoom(roomName);
			p.room = this.rooms[roomName];
		}

		p.room.addPlayerToRoom(p);
	}
}