/// <reference path="../../../node_modules/@types/node/index.d.ts"/>

import net = require("net");
process.title = "Transformice Server";

import { Bufferpack } from "./utils/bufferpack";
import { TransformiceServer } from "./Server";
import { TransformiceRoom } from "./Room";

// Server
const Server: TransformiceServer = new TransformiceServer();

export class TransformiceClient {
	
	conn: net.Socket;
	server: TransformiceServer;
	room: TransformiceRoom;
	connectionId: number;
	validandoVersao: boolean;

	constructor(conn: net.Socket) {
		this.conn = conn;
		this.server = Server;

		this.conn.on("connect", this.onConnected.bind(this));
		this.conn.on("data", this.onMessage.bind(this));
		this.conn.on("close", this.onDisconnect.bind(this));
	
		this.validandoVersao = true;
	}

	/**
	 *	Envia dados ao player
	 *  @param buffer	Buffer object ou string
	 */
	private write(buffer: any) {
		this.conn.write(buffer);
	}

	/**
	 *	Fecha a conexão
	 */
	private close() {
		this.conn.destroy();
	}

	/**
	 *	Quando algum player conrectar
	 */
	private onConnected() {
		this.server.onClientConnected(this);
	}

	/**
	 *	Quando alguém for desconectado
	 */
	private onDisconnect(ocorreuErro: boolean) {
		this.server.onClientDisconnected(this);
	}

	/**
	 *	Quando receber alguma mensagem
	 */
	private onMessage(data: Buffer) {
		if (this.validandoVersao) {
			if (data.toString() == "<policy-file-request/>\x00") {
				this.write('<cross-domain-policy><allow-access-from domain="*" to-ports="*" /></cross-domain-policy>\x00');
				this.close();				
				return;
			}

			var versao: any = data[6] << 8 | data[7];
				versao = `1.${versao}`;
			
			var ckeylen: number = data[8] << 8 | data[9];
			var ckey: string = data.slice(10, 10+ckeylen).toString();

			if (versao != this.server.version || ckey != this.server.ckey) {
				this.close();
				return;
			}

			this.validandoVersao = false;
			this.sendCorrectVersion();
		} else {
			/*
				Vc faz o resto daqui blz?
			*/
		}
	}

	/**
	 *	Send data
	 */
	private sendData(ev : number[], vals :Buffer) {
		var packetSize: number = ev.length + vals.byteLength;
		var packet: Bufferpack = new Bufferpack();

		if (packetSize <= 0xff) {
			packet.writeByte(1);
			packet.writeByte(packetSize);
		
		}  else if (packetSize <= 0xffff) {
			packet.writeByte(2);
			packet.writeInt16(packetSize);
		
		} else if (packetSize <= 0xffffff) {
			packet.writeByte(3);
			packet.writeByte(packetSize >> 18 & 255);
			packet.writeByte(packetSize >> 8 & 255);
			packet.writeByte(packetSize & 255);
		}

		packet.writeBytes(Buffer.concat([new Buffer(ev), vals]));		
		this.write(packet.toBuffer());
	}

	/**
	 *	Envia a versão para o player (para entrar na tela de login)
	 */
	private sendCorrectVersion() {
		// Correct version
		var buffer: Bufferpack = new Bufferpack();
		buffer.writeInt32(0);
		buffer.writeByte(0);
		buffer.writeInt16(2);
		buffer.writeString("br");
		buffer.writeInt16(2);
		buffer.writeString("br");
		buffer.writeInt32(0);
		this.sendData([0x1a, 0x03], buffer.toBuffer());

		// Imagem
		this.sendData([0x14, 0x04], new Buffer([0, 0]));

		// Banner
		buffer = new Bufferpack();
		buffer.writeInt16(564);
		buffer.writeByte(20); // banner id
		buffer.writeByte(1);
		buffer.writeBoolean(false);
		this.sendData([0x10, 0x09], buffer.toBuffer());
	}
}

let portas: number[] = [44444, 44440, 3724, 5555, 6112];
for (var i: number = 0; i < portas.length; i++) {
	net.createServer(function(c: net.Socket) {
		new TransformiceClient(c);
	}).listen(portas[i]);
	console.log(`Iniciado na porta: ${portas[i]}`);
}