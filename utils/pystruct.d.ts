declare module "pystruct" {
	export function pack(format: string, ...values: any[]) : Buffer;
	export function unpack(format: string, buffer: Buffer) : any[];
}