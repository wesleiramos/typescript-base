/// <reference path="pystruct.d.ts" />

import struct = require("pystruct");

export class Bufferpack {
	
	buffer: Buffer;
	position: number;

	/**
	 *	Novo buffer
	 */
	constructor(buffer?: Buffer) {

		if (buffer === undefined) this.buffer = Buffer.alloc(0);
		else this.buffer = buffer;

		this.position = 0;
	}

	/**
	 *	Retorna o buffer
	 */
	public toBuffer() : Buffer {
		return this.buffer;
	}

	/**
	 *	Retorna o tamanho
	 */
	public lenght() : number {
		return this.buffer.length;
	}

	/**
	 *	Define uma posição
	 */
	public setPosition(p: number) {
		this.position += p;
	}

	/**
	 *	Retorna a posição
	 */
	public getPosition() : number {
		return this.position;
	}

	/**
	 *	Escreve um boolean
	 */
	public writeBoolean(b: boolean) {
		if (b) {
			this.writeByte(1);
			return;
		}
		this.writeByte(0);
	}

	/**
	 *	Escreve um byte
	 */
	public writeByte(b: number) {
		this.buffer = Buffer.concat([this.buffer, struct.pack("!b", b)]);
	}

	/**
	 *	Escreve bytes
	 */
	public writeBytes(b: Buffer) {
		this.buffer = Buffer.concat([this.buffer, b]);
	}

	/**
	 *	Escreve uma string
	 */
	public writeString(s: string) {
		this.writeBytes(Buffer.alloc(s.length, s));
	}

	/**
	 *	Escreve um int de 16 bits
	 */
	public writeInt16(i: number) {
		this.buffer = Buffer.concat([this.buffer, struct.pack("!h", i)]);
	}

	/**
	 *	Escreve um int de 32 bits
	 */
	public writeInt32(i: number) {
		this.buffer = Buffer.concat([this.buffer, struct.pack("!i", i)]);
	}

	/**
	 *	Lê um byte
	 */
	public readByte() : number {
		var v: number = this.buffer.readInt8(this.position);
		this.setPosition(1);
		return v;
	}

	/**
	 *	Lê um boolean
	 */
	public readBoolean() : boolean {
		return this.readByte() != 0;
	}

	/**
	 *	Lê um int de 16 bits
	 */
	public readInt16() : number {
		var v: number = this.buffer.readInt16BE(this.position);
		this.setPosition(2);
		return v;
	}

	/**
	 *	Lê um int de 32 bits
	 */
	public readInt32() : number {
		var v: number = this.buffer.readInt32BE(this.position);
		this.setPosition(4);
		return v;
	}

	/**
	 *	Lê uma string
	 *	@param len tamanho
	 */
	public readString(len: number) : string {
		var v: string = this.buffer.toString("utf-8", this.position, this.position + len);
		this.setPosition(len);
		return v;
	}
}